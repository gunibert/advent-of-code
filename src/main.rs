mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;

use clap::Parser;

#[derive(Parser)]
struct Args {
    #[arg(short, long)]
    day: i32,
}

fn main() {
    let args = Args::parse();

    match args.day {
        1 => {
            day1::part1();
            day1::part2();
        }
        2 => {
            day2::part1();
            day2::part2();
        }
        3 => {
            day3::part1();
            day3::part2();
        }
        4 => {
            day4::part1();
            day4::part2();
        }
        5 => {
            day5::part1();
            day5::part2();
        }
        6 => {
            day6::part1();
        }

        _ => unimplemented!(),
    }
}
