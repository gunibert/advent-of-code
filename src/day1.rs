pub(crate) fn part1() {
    let input = include_str!("../data/day1.txt");
    let calories = find_buckets(input);

    // max
    let max_calories = calories.iter().max().unwrap();
    println!("{max_calories}");
}

pub(crate) fn part2() {
    let input = include_str!("../data/day1.txt");

    let mut calories = find_buckets(input);

    // find top three max
    calories.sort();
    calories.reverse();
    let sum_top_three_calories: i32 = calories.iter().take(3).sum();
    println!("{sum_top_three_calories}");
}

fn find_buckets(input: &str) -> Vec<i32> {
    let mut calories = vec![];

    let mut sum = 0;
    for line in input.lines() {
        if line.is_empty() {
            // reset
            calories.push(sum);
            sum = 0;
            continue
        }

        sum += line.parse::<i32>().unwrap();
    }
    calories
}
