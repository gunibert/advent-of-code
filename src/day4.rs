use nom::bytes::complete::tag;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::{map, map_res};
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::IResult;
use std::num::ParseIntError;

#[derive(Debug)]
struct Elf {
    start: u32,
    end: u32,
}

impl Elf {
    fn parse(input: &str) -> IResult<&str, Self> {
        let parser = nom::sequence::separated_pair(digit1, tag("-"), digit1);
        let result_parser = map_res(parser, |(start, end): (&str, &str)| {
            Ok::<(u32, u32), ParseIntError>((start.parse()?, end.parse()?))
        });
        map(result_parser, |(start, end)| Elf { start, end })(input)
    }

    fn contains_other(&self, other: &Self) -> bool {
        if other.start >= self.start && other.end <= self.end {
            return true;
        }
        false
    }

    fn overlap_other(&self, other: &Self) -> bool {
        if self.end >= other.start && self.start <= other.end {
            return true;
        }
        false
    }
}

fn parse_line(line: &str) -> IResult<&str, (Elf, Elf)> {
    separated_pair(Elf::parse, tag(","), Elf::parse)(line)
}

pub(crate) fn part1() {
    let input = include_str!("../data/day4.txt");
    let (_, elves) = separated_list1(line_ending, parse_line)(input).unwrap();
    let mut sum = 0;
    for (elf1, elf2) in elves {
        if elf1.contains_other(&elf2) || elf2.contains_other(&elf1) {
            sum += 1;
        }
    }
    println!("{sum}");
}

pub(crate) fn part2() {
    let input = include_str!("../data/day4.txt");
    let (_, elves) = separated_list1(line_ending, parse_line)(input).unwrap();
    let mut sum = 0;
    for (elf1, elf2) in elves {
        if elf1.overlap_other(&elf2) {
            sum += 1;
        }
    }
    println!("{sum}");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_elf_parser() {
        let input = "83-93";
        let elf = Elf::parse(input).unwrap().1;
        assert_eq!(elf.start, 83);
        assert_eq!(elf.end, 93);
    }

    #[test]
    fn test_overlap() {
        let testdata = [
            (2, 4, 6, 8, false),
            (2, 3, 4, 5, false),
            (4, 5, 2, 3, false),
            (5, 7, 7, 9, true),
            (2, 8, 3, 7, true),
            (6, 6, 4, 6, true),
            (2, 6, 4, 8, true),
        ];

        for item in testdata {
            let elf1 = Elf {
                start: item.0,
                end: item.1,
            };
            let elf2 = Elf {
                start: item.2,
                end: item.3,
            };

            assert_eq!(
                elf1.overlap_other(&elf2),
                item.4,
                "Elves overlap problem {elf1:?}: {elf2:?}"
            );
        }
    }
}
