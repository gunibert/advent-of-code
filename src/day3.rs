use anyhow::anyhow;

struct Rucksack {
    content: String,
}

impl Rucksack {
    fn new<T: AsRef<str>>(content: T) -> Self {
        Self {
            content: content.as_ref().to_string(),
        }
    }

    fn common_item(&self) -> Result<Item, anyhow::Error> {
        // code to find the duplicate in both compartments
        let half = self.content.len() / 2;
        let first_compartment: Vec<_> = self.content.chars().take(half).collect();
        let second_compartment: Vec<_> = self.content.chars().skip(half).take(half).collect();
        for i in first_compartment {
            if second_compartment.contains(&i) {
                return Ok(Item(i));
            }
        }
        Err(anyhow!("Could not find a common item in both compartments"))
    }
}

struct Item(char);

impl Item {
    fn score(&self) -> u32 {
        if self.0.is_lowercase() {
            return self.0 as u32 - 'a' as u32 + 1;
        }
        self.0 as u32 - 'A' as u32 + 27
    }
}

fn common_badge(r1: &Rucksack, r2: &Rucksack, r3: &Rucksack) -> Result<Item, anyhow::Error> {
    let r1_v: Vec<_> = r1.content.chars().collect();
    let r2_v: Vec<_> = r2.content.chars().collect();
    let r3_v: Vec<_> = r3.content.chars().collect();
    for c in r1_v {
        for d in &r2_v {
            if r3_v.contains(&c) && r3_v.contains(&d) && r2_v.contains(&c) {
                return Ok(Item(c));
            }
        }
    }
    Err(anyhow!("Could not find a common item all 3 rucksacks"))
}

pub(crate) fn part1() {
    let input = include_str!("../data/day3.txt");
    let rucksacks: Vec<_> = input.lines().map(|line| Rucksack::new(line)).collect();
    let score_part1: u32 = rucksacks
        .iter()
        .map(|rucksack| rucksack.common_item())
        .flatten()
        .map(|i| i.score())
        .sum();
    println!("{score_part1}");
}

pub(crate) fn part2() {
    let input = include_str!("../data/day3.txt");
    let rucksacks: Vec<_> = input.lines().map(|line| Rucksack::new(line)).collect();
    let score_part2: u32 = rucksacks
        .chunks(3)
        .map(|chunk| common_badge(&chunk[0], &chunk[1], &chunk[2]))
        .flatten()
        .map(|i| i.score())
        .sum();
    println!("{score_part2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_score() {
        let item_a = Item('a');
        assert_eq!(item_a.score(), 1);
        let item_big_a = Item('A');
        assert_eq!(item_big_a.score(), 27);
    }

    #[test]
    fn test_identifying_the_common_item() {
        let rucksack = Rucksack::new("vJrwpWtwJgWrhcsFMMfFFhFp");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 16);

        let rucksack = Rucksack::new("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 38);

        let rucksack = Rucksack::new("PmmdzqPrVvPwwTWBwg");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 42);

        let rucksack = Rucksack::new("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 22);

        let rucksack = Rucksack::new("ttgJtRGJQctTZtZT");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 20);

        let rucksack = Rucksack::new("CrZsJsPPZsGzwwsLwLmpwMDw");
        let item = rucksack.common_item().unwrap();
        assert_eq!(item.score(), 19);
    }

    #[test]
    fn test_common_in_all_three() {
        let rucksack1 = Rucksack::new("vJrwpWtwJgWrhcsFMMfFFhFp");
        let rucksack2 = Rucksack::new("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL");
        let rucksack3 = Rucksack::new("PmmdzqPrVvPwwTWBwg");

        let badge = common_badge(&rucksack1, &rucksack2, &rucksack3).unwrap();
        assert_eq!(badge.0, 'r');
    }
}
