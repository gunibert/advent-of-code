use std::collections::HashSet;

pub(crate) fn part1() {
    let input = include_str!("../data/day6.txt");
    let start_of_packet = find_start_of_packet(input);
    println!("{start_of_packet}");

    let start_of_message = find_start_of_message(input);
    println!("{start_of_message}");
}

fn find_start_of_packet(input: &str) -> u32 {
    let chars: Vec<_> = input.chars().collect();
    let windows: Vec<_> = chars.windows(4).enumerate().collect();
    for (i, window) in windows {
        if unique(window, 4) {
            return 4 + i as u32;
        }
    }
    0
}

fn find_start_of_message(input: &str) -> u32 {
    let chars: Vec<_> = input.chars().collect();
    let windows: Vec<_> = chars.windows(14).enumerate().collect();
    for (i, window) in windows {
        if unique(window, 14) {
            return 14 + i as u32;
        }
    }
    0
}

fn unique(window: &[char], unqiue_len: usize) -> bool {
    let set: HashSet<&char> = HashSet::from_iter(window);
    if set.len() == unqiue_len {
        return true;
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_first_example() {
        let input = "bvwbjplbgvbhsrlpgdmjqwftvncz";
        let start_of_packet = find_start_of_packet(input);
        assert_eq!(start_of_packet, 5);

        let start_of_message = find_start_of_message(input);
        assert_eq!(start_of_message, 23);
    }

    #[test]
    fn test_second_example() {
        let input = "nppdvjthqldpwncqszvftbrmjlhg";
        let start_of_packet = find_start_of_packet(input);
        assert_eq!(start_of_packet, 6);

        let start_of_message = find_start_of_message(input);
        assert_eq!(start_of_message, 23);
    }

    #[test]
    fn test_third_example() {
        let input = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
        let start_of_packet = find_start_of_packet(input);
        assert_eq!(start_of_packet, 10);

        let start_of_message = find_start_of_message(input);
        assert_eq!(start_of_message, 29);
    }

    #[test]
    fn test_fourth_example() {
        let input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";
        let start_of_packet = find_start_of_packet(input);
        assert_eq!(start_of_packet, 11);

        let start_of_message = find_start_of_message(input);
        assert_eq!(start_of_message, 26);
    }
}