use std::str::FromStr;

pub enum Shape {
    Rock,
    Scissors,
    Paper,
}

impl Shape {
    fn get_points(self) -> i32 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }
}

pub enum Outcome {
    Draw,
    Win,
    Loss,
}

impl Outcome {
    fn get_points(&self) -> i32 {
        match self {
            Outcome::Draw => 3,
            Outcome::Win => 6,
            Outcome::Loss => 0,
        }
    }
}

impl FromStr for Shape {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        return match s {
            "A" | "X" => Ok(Self::Rock),
            "B" | "Y" => Ok(Self::Paper),
            "C" | "Z" => Ok(Self::Scissors),
            _ => Err(()),
        };
    }
}

impl FromStr for Outcome {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        return match s {
            "X" => Ok(Self::Loss),
            "Y" => Ok(Self::Draw),
            "Z" => Ok(Self::Win),
            _ => Err(()),
        };
    }
}

pub fn calculate_score(opponent: Shape, me: Shape) -> i32 {
    return match (opponent, me) {
        (Shape::Rock, Shape::Rock) => 1 + 3,
        (Shape::Scissors, Shape::Rock) => 1 + 6,
        (Shape::Paper, Shape::Rock) => 1 + 0,
        (Shape::Rock, Shape::Paper) => 2 + 6,
        (Shape::Scissors, Shape::Paper) => 2 + 0,
        (Shape::Paper, Shape::Paper) => 2 + 3,
        (Shape::Rock, Shape::Scissors) => 3 + 0,
        (Shape::Scissors, Shape::Scissors) => 3 + 3,
        (Shape::Paper, Shape::Scissors) => 3 + 6,
    };
}

pub fn calculate_score_part2(opponent: Shape, outcome: Outcome) -> i32 {
    let outcome_points = outcome.get_points();
    return match (opponent, outcome) {
        (Shape::Rock, Outcome::Win) => Shape::Paper.get_points() + outcome_points,
        (Shape::Scissors, Outcome::Win) => Shape::Rock.get_points() + outcome_points,
        (Shape::Paper, Outcome::Win) => Shape::Scissors.get_points() + outcome_points,
        (Shape::Rock, Outcome::Loss) => Shape::Scissors.get_points() + outcome_points,
        (Shape::Scissors, Outcome::Loss) => Shape::Paper.get_points() + outcome_points,
        (Shape::Paper, Outcome::Loss) => Shape::Rock.get_points() + outcome_points,
        (Shape::Rock, Outcome::Draw) => Shape::Rock.get_points() + outcome_points,
        (Shape::Scissors, Outcome::Draw) => Shape::Scissors.get_points() + outcome_points,
        (Shape::Paper, Outcome::Draw) => Shape::Paper.get_points() + outcome_points,
    };
}

pub(crate) fn part1() {
    let input = include_str!("../data/day2.txt");
    let mut sum = 0;
    for line in input.lines() {
        let raw_hands: Vec<_> = line.split(' ').collect();
        let opponent = Shape::from_str(raw_hands[0]).expect("Could not create Shape");
        let me = Shape::from_str(raw_hands[1]).expect("Could not create Shape");
        sum += calculate_score(opponent, me);
    }
    println!("{sum}");
}

pub(crate) fn part2() {
    let input = include_str!("../data/day2.txt");
    let mut sum = 0;
    for line in input.lines() {
        let raw_hands: Vec<_> = line.split(' ').collect();
        let opponent = Shape::from_str(raw_hands[0]).expect("Could not create Shape");
        let me = Outcome::from_str(raw_hands[1]).expect("Could not create Outcome");
        sum += calculate_score_part2(opponent, me);
    }
    println!("{sum}");
}
