use nom::bytes::complete::tag;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::{map, map_res};
use nom::multi::separated_list1;
use nom::sequence::tuple;
use nom::IResult;

struct Movement {
    quantity: usize,
    start: usize,
    end: usize,
}

impl Movement {
    fn parse_number(input: &str) -> IResult<&str, usize> {
        map_res(digit1, |i: &str| i.parse::<usize>())(input)
    }

    fn parse_movement(input: &str) -> IResult<&str, Self> {
        let parser = tuple((
            tag("move "),
            Movement::parse_number,
            tag(" from "),
            Movement::parse_number,
            tag(" to "),
            Movement::parse_number,
        ));

        map(parser, |(_, quantity, _, start, _, end)| Self {
            quantity,
            start,
            end,
        })(input)
    }
}

pub(crate) fn part1() {
    let input = include_str!("../data/day5_stripped.txt");
    let (_, movements) = separated_list1(line_ending, Movement::parse_movement)(input).unwrap();

    let mut buckets: [Vec<&str>; 9] = Default::default();
    initial_state(&mut buckets);

    for movement in movements {
        for _ in 0..movement.quantity {
            let thing = buckets[movement.start - 1].pop();
            if let Some(thing) = thing {
                buckets[movement.end - 1].push(thing);
            }
        }
    }

    for i in 0..9 {
        let a = buckets[i].pop().unwrap();
        print!("{a}");
    }
    println!("\n");
}

pub(crate) fn part2() {
    let input = include_str!("../data/day5_stripped.txt");
    let (_, movements) = separated_list1(line_ending, Movement::parse_movement)(input).unwrap();

    let mut buckets: [Vec<&str>; 9] = Default::default();
    initial_state(&mut buckets);

    for movement in movements {
        let mut tmp = vec![];
        for _ in 0..movement.quantity {
            // pop all
            tmp.push(buckets[movement.start - 1].pop().unwrap());
        }

        for _ in 0..movement.quantity {
            let thing = tmp.pop();
            if let Some(thing) = thing {
                buckets[movement.end - 1].push(thing);
            }
        }
    }

    for i in 0..9 {
        let a = buckets[i].pop().unwrap();
        print!("{a}");
    }
}

fn initial_state(buckets: &mut [Vec<&str>; 9]) {
    buckets[0] = vec!["N", "Q", "L", "S", "C", "Z", "P", "T"];
    buckets[1] = vec!["G", "C", "H", "V", "T", "P", "L"];
    buckets[2] = vec!["F", "Z", "C", "D"];
    buckets[3] = vec!["C", "V", "M", "L", "D", "T", "W", "G"];
    buckets[4] = vec!["C", "W", "P"];
    buckets[5] = vec!["Z", "S", "T", "C", "D", "J", "F", "P"];
    buckets[6] = vec!["D", "B", "G", "W", "V"];
    buckets[7] = vec!["W", "H", "Q", "S", "J", "N"];
    buckets[8] = vec!["V", "L", "S", "F", "Q", "C", "R"];

    for bucket in buckets {
        bucket.reverse();
    }
}
